# Run Basecamp w/ Harvest as an Mac Application

This is a _hack_ of Chromium Portable designed to act like a Basecamp App, when you run it, it will pull up Basecamp.

## Installation

- Goto a folder, e.g. `~/Downloads`, don't start off in the `/Applications` folder!
- Clone this repo e.g:

```
git clone https://bitbucket.org/aubreypwd/basecamp-harvest-mac-app Basecamp.app
```

- When done move to `/Applications`
- Run `Basecamp.app`, you should see Basecamp load up (Harvest will not be installed)
- Open a new Chromium Window using `CTRL + N`
- Install [Harvest Extension for Chrome](https://chrome.google.com/webstore/detail/harvest-time-tracker/fbpiglieekigmkeebmeohkelfpjjlaia)
- Close Window
- Refresh Basecamp, you should see Timer
